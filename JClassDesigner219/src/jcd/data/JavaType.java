/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

/**
 *
 * @author Mrunal
 */
public abstract class JavaType implements Comparable<JavaType> {
    protected VBox box;
    static final String VBOX_PROPS = "-fx-background-color: white; -fx-border-color: black; -fx-border-width: 1 0 0 0;";
    protected Text pkg;
    protected ArrayList<String> dependencies;
    protected boolean isAbstract;
    protected Text name;
    protected JavaType parentJT;
    protected String parent;
    protected ArrayList<InterfaceJT> interfaces;
    protected ArrayList<VariableJT> vars;
    protected ArrayList<MethodJT> methods;
    protected boolean isValid = true;
    
    protected Line right;
    protected VBox nbox;
    protected VBox vbox;
    protected VBox mbox;
    
    protected boolean relevant;
    protected ArrayList<JavaType> refrences;
    
    public abstract String exportToCode();
    
    @Override
    public int compareTo(JavaType t) {
        int pkgcmp = this.getPkg().compareTo(t.getPkg());
        if(pkgcmp == 0) {
            int namecmp = this.getName().compareTo(t.getName());
            return namecmp;
        } else {
            return pkgcmp;
        }
    }
    
    public VBox getBox() {
        return box;
    }
    
    public Line getRight() {
        return right;
    }
    //PROPERTIES
    public void addRef(JavaType ref) {
        if(ref!=null&&!refrences.contains(ref)) {
            refrences.add(ref);
        }
    }
    public void remRef(JavaType ref) {
        refrences.remove(ref);
    }
    public ArrayList<JavaType> getRefrences() {
        return refrences;
    }
    
    public boolean isRelevant() {
        return relevant;
    }
    
    public void addDependency(String dependency) {
        dependencies.add(dependency);
    }
    
    public void removeDependency(String dependency) {
        dependencies.remove(dependency);
    }
    
    public ArrayList<String> getDependencies() {
        return dependencies;
    }
    
    public void addInterface(InterfaceJT element) {
        interfaces.add(element);
    }
    
    public void removeInterface(InterfaceJT element) {
        interfaces.remove(element);
    }
    
    public VariableJT findVariable(String target) {
        for(VariableJT var : vars) {
            if(var.exportToCode().trim().equals(target)) {
                return var;
            }
        }
        return null;
    }
    
    public boolean validVariable(String name) {
        for(VariableJT var : vars) {
            if(var.compareTo(name)==0) {
                return false;
            }
        }
        return true;
    }
    
    public void addVariable(VariableJT var) {
        if(vars.size() == 0 && methods.size() == 0) {
            box.getChildren().add(vbox);
            nbox.setStyle(VBOX_PROPS+"-fx-border-width: 2 2 0 2;");
            vbox.setStyle(VBOX_PROPS+"-fx-border-width: 1 2 2 2;");
        }
        vars.add(var);
        Text varText = new Text(var.toString());
        vbox.getChildren().add(varText);
    }
    
    public void removeVariable(VariableJT var) {
        vars.remove(var);
        for(int i = 0; i < vbox.getChildren().size(); i++) {
            Text text = (Text) vbox.getChildren().get(i);
            if(text.getText().equals(var.toString())) {
                vbox.getChildren().remove(i);
                break;
            }
        }
        if(vars.size() == 0 && methods.size() == 0) {
            box.getChildren().remove(vbox);
            nbox.setStyle(VBOX_PROPS+"-fx-border-width: 2;");
        }
    }
    
    public MethodJT findMethod(String target) {
        for(MethodJT method : methods) {
            if(method.exportToCode().trim().equals(target)) {
                return method;
            }
        }
        return null;
    }
    
    public void addMethod(MethodJT method) {
        if(vars.size() == 0 && methods.size() == 0) {
            box.getChildren().add(vbox);
            box.getChildren().add(mbox);
            nbox.setStyle(VBOX_PROPS+"-fx-border-width: 2 2 0 2;");
            vbox.setStyle(VBOX_PROPS+"-fx-border-width: 1 2 0 2;");
            mbox.setStyle(VBOX_PROPS+"-fx-border-width: 1 2 2 2;");
        } else if(vars.size() > 0 && methods.size() == 0) {
            box.getChildren().add(mbox);
            vbox.setStyle(VBOX_PROPS+"-fx-border-width: 1 2 0 2;");
            mbox.setStyle(VBOX_PROPS+"-fx-border-width: 1 2 2 2;");
        }
        methods.add(method);
        Text methodText = new Text(method.toString());
        if(method.isIsAbstract()) {
            isAbstract = true;
            methodText.setStyle("-fx-font-style: italic;");
        }
        mbox.getChildren().add(methodText);
    }
    
    public void removeMethod(MethodJT method) {
        methods.add(method);
        mbox.getChildren().remove(method);
        if(methods.size() == 0) {
            box.getChildren().remove(mbox);
            vbox.setStyle(VBOX_PROPS+"-fx-border-width: 1 2 2 2;");
            if(vars.size() == 0) {
                box.getChildren().remove(vbox);
                nbox.setStyle(VBOX_PROPS+"-fx-border-width: 2;");
            }
        }
    }
    
    public String getName() {
        return name.getText();
    }
    
    public void setName(String newName) {
        name.setText(newName);
    }
    
    public String getPkg() {
        return pkg.getText();
    }
    
    public void setPkg(String newPkg) {
        pkg.setText(newPkg);
    }
    
    public boolean getAbstract() {
        return isAbstract;
    }
    
    public void setAbstract(boolean abstractProp) {
        isAbstract = abstractProp;
    }
    
    public ArrayList<VariableJT> getVars() {
        return vars;
    }
    
    public ObservableList getVarsData() {
        return FXCollections.observableArrayList(vars);
    }
    
    public ArrayList<String> getVarNames() {
        ArrayList<String> names = new ArrayList();
        for(VariableJT var : vars) {
            names.add(var.exportToCode().trim());
        }
        return names;
    }
    
    public void setVars(ArrayList<VariableJT> vars) {
        this.vars = vars;
    }
    
    public ArrayList<MethodJT> getMethods() {
        return methods;
    }
    
    public ObservableList getMethodsData() {
        return FXCollections.observableArrayList(methods);
    }
    
    public ArrayList<String> getMethodNames() {
        ArrayList<String> names = new ArrayList();
        for(MethodJT method : methods) {
            names.add(method.exportToCode().trim());
        }
        return names;
    }
    public void setMethods(ArrayList<MethodJT> methods) {
        this.methods = methods;
    }
    
    public String getParent() {
        return parent;
    }
    
    public JavaType getParentJT() {
        return parentJT;
    }

    public void setParentJT(JavaType parent) {
        if(parent == null) {
            this.parent = "";
        }else {
            this.parent = parent.getName();
        }
        parentJT = parent;
    }

    public ArrayList<InterfaceJT> getInterfaces() {
        return interfaces;
    }

    public void setInterfaces(ArrayList<InterfaceJT> interfaces) {
        this.interfaces = interfaces;
    }
    
    public ArrayList<String> getInterfaceNames() {
        ArrayList<String> names = new ArrayList();
        for(InterfaceJT item : interfaces) {
            names.add(item.getName());
        }
        return names;
    }
    //VISUAL EFFECTS
    public void resizeModeOn() {
        right.setMouseTransparent(false);
    }
    
    public void resizeModeOff() {
        right.setMouseTransparent(true);
    }
    
    public boolean getValidity() {
        return isValid;
    }
    
    public void setValidity(boolean valid) {
        isValid = valid;
        DropShadow e = new DropShadow();
        e.setOffsetX(0);
        e.setOffsetY(0);
        e.setRadius(10);
        if(!isValid) {
            e.setColor(Color.RED);
        } else {
            e.setColor(Color.YELLOW);
        }
        box.setEffect(e);
    }
    
    public void selectEffectOn() {
        DropShadow e = new DropShadow();
        e.setOffsetX(0);
        e.setOffsetY(0);
        e.setRadius(10);
        if(isValid) {
            e.setColor(Color.YELLOW);
        } else {
            e.setColor(Color.ORANGE);
        }
        box.setEffect(e);
    }
    
    public void selectEffectOff() {
        DropShadow e = null;
        if(!isValid) {
            e = new DropShadow();
            e.setOffsetX(0);
            e.setOffsetY(0);
            e.setRadius(10);
            e.setColor(Color.RED);  
        }
        box.setEffect(e);
    }
    
    public double getX() {
        return box.layoutXProperty().get();
    }
    
    public double getY() {
        return box.layoutXProperty().get();
    }
    
    public void setX(double newX) {
        box.setLayoutX(newX);
    }
    
    public void setY(double newY) {
        box.setLayoutY(newY);
    }

}