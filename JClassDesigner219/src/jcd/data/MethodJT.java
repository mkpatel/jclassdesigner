/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;

/**
 *
 * @author Mrunal
 */
public class MethodJT {
    private String type;
    private String name;
    private String visibility;
    private boolean isStatic;
    private boolean isAbstract;
    private ArrayList<VariableJT> params;
    
    //FIX FOR HANDLING ONLY ABSTRACT OR STATIC
    //CONSTRUCTORS
    public MethodJT() {
        this("type","name","default",true,false,new ArrayList<VariableJT>());
    }
    public MethodJT(String type, String name, String visibility, boolean isStatic, boolean isAbstract, ArrayList<VariableJT> params) {
        this.type = type;
        this.name = name;
        this.visibility = visibility;
        this.isStatic = isStatic;
        this.isAbstract = isAbstract;
        this.params = params;
    }
    
    //PROPERTIES
    public void setName(String newName) {
        name = newName;
    }
    public String getName() {
        return name;
    }
    public void setType(String newType) {
        type = newType;
    }
    public String getType() {
        return type;
    }
    public void setVisibility(String newVisibility) {
        visibility = newVisibility;
    }
    public String getVisibility() {
        return visibility;
    }
    public void setStatic(boolean newStatic) {
        isStatic = newStatic;
    }
    public boolean isIsStatic() {
        return isStatic;
    }
    public void setParams(ArrayList<VariableJT> newParams) {
        params = newParams;
    }
    public ArrayList<VariableJT> getParams() {
        return params;
    }
    public void setAbstract(boolean newAbstract) {
        isAbstract = newAbstract;
    }
    public boolean isIsAbstract() {
        return isAbstract;
    }
    
    public String exportToCode() {
        String method = type+" "+name+"(";
        for(int i = 0; i < params.size(); i++) {
            method += params.get(i).asParam();
            if(i < params.size()-1)
                method += ", ";
        }
        if(isAbstract) {
            method = "abstract " + method;
        } 
        if(isStatic) {
            method = "static " + method;
        } 
        switch(visibility) {
            case "public":
            case "private":
            case "protected":
                method = visibility+" "+method;
        }
        if(type.equals("void")) {
            method = method + "){\n\n}";
        } else if(type.equals("int")||type.equals("char")||type.equals("short")
                ||type.equals("long")||type.equals("byte")) {
            method = method + "){\n\t\treturn 0;\n\t}";
        } else if(type.equals("double")||type.equals("float")) {
            method = method + "){\n\t\treturn 0.0;\n\t}";
        } else if(type.equals("boolean")) {
            method = method + "){\n\t\treturn false;\n\t}";
        } else {
            method = method + "){\n\t\treturn null;\n\t}";
        } 
        return "\t"+method+"\n";
    }
    
    @Override
    public String toString() {
        String part1 = name+"(";
        for(int i = 0; i < params.size(); i++) {
            part1 += params.get(i).toString();
            if(i < params.size()-1)
                part1 += ", ";
        }
        String initial = part1+") : "+type;
        if(isStatic) {
            initial = "$"+initial;
        }
        switch(visibility) {
            case "public":
                initial = "+"+initial;
            break;
            case "private":
                initial = "-"+initial;
            break;
            case "protected":
                initial = "#"+initial;
            break;
        }
        return initial;
    }
}