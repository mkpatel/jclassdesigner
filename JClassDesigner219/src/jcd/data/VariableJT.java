/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

/**
 *
 * @author Mrunal
 */
public class VariableJT implements Comparable<String> {
    private String type;
    private String name;
    private String visibility;
    private boolean isStatic;
    private boolean isFinal;

    //CONSTRUCTORS
    //PARAM CONSTRUCTOR
    public VariableJT(String type, String name) {
        this(type,name,"",false,false);
    }
    //REGULAR CONSTRUCTOR
    public VariableJT(String type, String name, String visibility, boolean isStatic, boolean isFinal) {
        this.type = type;
        this.name = name;
        this.visibility = visibility;
        this.isStatic = isStatic;
        this.isFinal = isFinal;
    }
    
    //PROPERTIES
    public void setName(String newName) {
        name = newName;
    }
    public String getName() {
        return name;
    }
    public void setType(String newType) {
        type = newType;
    }
    public String getType() {
        return type;
    }
    public void setVisibility(String newVisibility) {
        visibility = newVisibility;
    }
    public String getVisibility() {
        return visibility;
    }
    public void setStatic(boolean newStatic) {
        isStatic = newStatic;
    }
    public boolean isIsStatic() {
        return isStatic;
    }
    public void setFinal(boolean newFinal) {
        isFinal = newFinal;
    }
    public boolean isIsFinal() {
        return isFinal;
    }
    
    public String asParam() {
        return getType() +" "+getName();
    }
    
    public String exportToCode() {       
        if(isFinal) {
            name = name.toUpperCase();
        }
        String var = type+" "+name+";";
        if(isFinal) {
            var = "final " + var;
        }
        if(isStatic) {
            var = "static "+var;
        } 
        switch(visibility) {
            case "public":
            case "private":
            case "protected":
                var = visibility+" "+var;
            break;
        }
        return "\t"+var+"\n";
    }
    
    @Override
    public String toString() {
        if(isFinal) {
            name = name.toUpperCase();
        }
        String initial = name+" : "+type;
        if(isStatic) {
            initial = "$"+initial;
        }
        switch(visibility) {
            case "public":
                initial = "+"+initial;
            break;
            case "private":
                initial = "-"+initial;
            break;
            case "protected":
                initial = "#"+initial;
            break;
        }
        return initial;
    }

    @Override
    public int compareTo(String t) {
        return name.compareTo(t);
    }
    
}
