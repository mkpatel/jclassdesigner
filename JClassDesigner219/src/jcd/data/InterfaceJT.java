/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import static jcd.data.JavaType.VBOX_PROPS;

/**
 *
 * @author Mrunal
 */
public class InterfaceJT extends JavaType {
    private final String INTERFACE = "«interface»";
    private static int counter = 1;
    
    public static void reset() {
        counter = 1;
    }
    
    public InterfaceJT(double x, double y, String n,boolean relevant) {
        this("",n,null,null,null,null,null,x,y,relevant);
    }
    public InterfaceJT(String pkg, String n, ArrayList<String> d, JavaType p, ArrayList<InterfaceJT> i, ArrayList<MethodJT> m, ArrayList<VariableJT> v, double x, double y,boolean relevant) {
        //PROPERTIES
        this.relevant = relevant;
        refrences = new ArrayList();
        box = new VBox();
        setX(x);
        setY(y);
        this.pkg = new Text(pkg);
        Text interfaceText = new Text(INTERFACE);
        if(n==null){
            n="Interface"+counter++;
        }
        name = new Text(n);
        name.setStyle("-fx-font-weight: bold;");
        interfaceText.setStyle("-fx-font-weight: bold;");
        parentJT = p;
        if(p != null) {
            parent = p.getName();
        } else {
            parent = "";
        }
        this.isAbstract = isAbstract;
        if(d==null) {
            d = new ArrayList();
        }
        this.dependencies = d;
        if(i==null) {
           i = new ArrayList();
        }
        this.interfaces = i;
        if(m==null) {
            m = new ArrayList();
        }
        methods = m;
        if(v==null) {
            v = new ArrayList();
        }
        vars = v;
        //GUI INIT
        nbox = initDefault(new VBox());
        vbox = initDefault(new VBox());
        mbox = initDefault(new VBox());
        nbox.getChildren().addAll(interfaceText, name);
        nbox.setAlignment(Pos.CENTER);
        if(m.size() > 0 || v.size() > 0) {
            box.getChildren().addAll(nbox,vbox,mbox);
        } else {
            box.getChildren().add(nbox);
        }
        vbox.setStyle(VBOX_PROPS);
        mbox.setStyle(VBOX_PROPS);
        nbox.setStyle(VBOX_PROPS+"-fx-border-width: 2;");
        for(VariableJT var : v) {
            addVariable(var);
        }
        for(MethodJT method : m) {
            addMethod(method);
        }
        
        right = new Line();
        right.setStroke(Color.TRANSPARENT);
        right.setStrokeWidth(4);
        
        right.startXProperty().bind(box.layoutXProperty().add(box.widthProperty()));
        right.endXProperty().bind(right.startXProperty());
        
        right.startYProperty().bind(box.layoutYProperty());
        right.endYProperty().bind(right.startYProperty().add(box.heightProperty()));

        right.setOnMouseEntered(e->{
            right.setCursor(Cursor.H_RESIZE);
        });
        right.setOnMouseExited(e->{
            right.setCursor(Cursor.DEFAULT);
        });
        right.setOnMouseDragged(e->{
            double newWidth = e.getX() - this.getBox().getLayoutX();
            this.getBox().setPrefWidth(newWidth);
        });
    }
    
    private VBox initDefault(VBox box) {
        box.setMinSize(120, 40);
        return box;
    }
    
    @Override
    public String exportToCode() {
        String str = "";
        if(!pkg.getText().isEmpty()) {
            str = "package " + pkg.getText() + ";\n\n";
        }
        for(String s : dependencies) {
            str += "import " + s+";\n";
        }
        str += "\n";
        str += "public ";
        str += "interface "+name.getText();
        if(parent!=null&&!parent.equals("")) {
            str += " extends " + parent;
        }
        if(interfaces.size()>0) {
            str += " implements ";
            for(int i = 0; i < interfaces.size(); i++) {
            str += interfaces.get(i).getName();
            if(i < interfaces.size()-1)
                str += ", ";
            }    
        }
        str+=" {\n\n";
        for(VariableJT v : vars)
            str += v.exportToCode();
        str += "\n";
        for(MethodJT m : methods) 
            str += m.exportToCode();
        str += "\n";
        str += "}";
        return str;
    }
    
    
}
