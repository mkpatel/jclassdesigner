/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.data;

import java.util.ArrayList;
import javafx.scene.Node;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import jcd.controller.CanvasController;
import jcd.gui.Workspace;
import jcd.data.AppState;
import saf.components.AppDataComponent;
import saf.AppTemplate;

/**
 *
 * @author Mrunal
 */
public class DataManager implements AppDataComponent {
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    //INFO
    AppState state;
    boolean snapMode;
    //ELEMENT DATA
    JavaType selected;
    ArrayList<JavaType> elements;
    ArrayList<String>   pkgNames;
    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) {
	// KEEP THE APP FOR LATER
	app = initApp;
        elements = new ArrayList<>();
        pkgNames = new ArrayList<>();
    }
    
    //APP INFO
    public AppState getState() {
	return state;
    }

    public void setState(AppState newState) {
	state = newState;
        if(!isInState(AppState.SELECT_MODE)) {
            if(selected!=null) {
                selected.selectEffectOff();
                selected = null;
            }
        }
    }

    public boolean isInState(AppState testState) {
	return state == testState;
    }
    
    public void setSnapMode(boolean snap) {
        snapMode = snap;
    }
    
    public boolean getSnapMode() {
        return snapMode;
    }
    
    //CLASS/INTERFACE DATA
    public JavaType find(VBox box) {
        for(JavaType item : elements) {
            if(box == item.getBox()) {
                return item;
            }
        }
        return null;
    }
    
    public void addElement(JavaType element) {
        elements.add(element);
        pkgNames.add(element.getPkg());
        updateGUI();
    }
    
    public void removeElement() {
        if(selected!=null) {
            JavaType current = selected;
            Workspace workspace = (Workspace)app.getWorkspaceComponent();
            workspace.getCanvas().getChildren().remove(selected.getRight());
            workspace.getCanvas().getChildren().remove(selected.getBox());
            elements.remove(selected);
            ArrayList<JavaType> targets = new ArrayList();
            for(JavaType item : elements) {
                item.remRef(current);
                if(!item.isRelevant()&&item.getRefrences().isEmpty()) {
                    workspace.getCanvas().getChildren().remove(item.getRight());
                    workspace.getCanvas().getChildren().remove(item.getBox());
                    targets.add(item);
                }
            }
            for(JavaType item : targets) {
                elements.remove(item);
            }
        }
        selected = null;
        updateGUI();
    }
    
    public void setSelected(JavaType current) {
        selected = current;
        updateGUI();
    }
    
    public JavaType getSelected() {
        return selected;
    }
    
    public boolean isValidJavaType() {
        for(JavaType element : elements) {
            if(element!=selected&&element.compareTo(selected)==0) {
                return false;
            }
        }
        return true;
    }
    
    public ArrayList<JavaType> getElements() {
        return elements;
    }
    
    public ArrayList<Node> getGUIElements() {
        ArrayList<Node> guielements = new ArrayList();
        for(JavaType item : elements) {
            guielements.add(item.getBox());
            guielements.add(item.getRight());
        }
        return guielements;
    }
    
    public void setPkg(String pkgName) {
        pkgNames.remove(selected.getPkg());
        selected.setPkg(pkgName);
        pkgNames.add(pkgName);
    }
    
    public ArrayList<String> getUniquePkgs() {
        ArrayList<String> targets = new ArrayList();
        for(String pkg : pkgNames) {
            if(!targets.contains(pkg)) {
                targets.add(pkg);
            }
        }
        return targets;
    } 
    
    public ArrayList<JavaType> findByPkg(String pkg) {
        ArrayList<JavaType> targets = new ArrayList();
        for(JavaType element : elements) {
            if(element.getPkg().equals(pkg)) {
                targets.add(element);
            }
        }
        return targets;
    }
    
    public void addInterface(String name) {
        for(JavaType item : elements) {
            if(item instanceof InterfaceJT && name.equals(item.getName())) {
                selected.addInterface((InterfaceJT)item);
                item.addRef(selected);
                Workspace workspace = (Workspace) app.getWorkspaceComponent();
                Text interfaceName = new Text(item.getName());
                workspace.getInterfaceList().getChildren().add(interfaceName);
                break;
            }
        }
        updateGUI();
    }
    public void removeInterface(String name) {
        for(JavaType item : elements) {
            if(item instanceof InterfaceJT && name.equals(item.getName())) {
                selected.removeInterface((InterfaceJT)item);
                item.remRef(selected);
                Workspace workspace = (Workspace) app.getWorkspaceComponent();
                for(Node n : workspace.getInterfaceList().getChildren()) {
                    if(((Text)n).getText().equals(name)) {
                       workspace.getInterfaceList().getChildren().remove(n);
                       break;
                    }
                }
                break;
            }
        }
        updateGUI();
    }
    
    public ArrayList<String> getInterfaceNames() {
        ArrayList<String> names = new ArrayList();
        for(JavaType item : elements) {
            if(item instanceof InterfaceJT && item != selected) {
                names.add(item.getName());
            }
        }
        return names;
    }
    
    public ArrayList<String> getParentNames() {
        ArrayList<String> names = new ArrayList();
        for(JavaType item : elements) {
            if(item != selected && item.getParentJT() != selected) {
                names.add(item.getName());
            }
        }
        if(selected!=null){
            if(!names.contains(selected.getParent())&&!selected.getParent().equals("")) {
                names.add(selected.getParent());
            }
        }
        return names;
    }
    
    public void setParent(String name) {
        if(selected != null) {
            if(name.equals("None")) {
                selected.setParentJT(null);
                for(JavaType item : elements) {
                    if(item.getRefrences().contains(selected)) {
                        item.remRef(selected);
                    }
                }
                ArrayList<JavaType> targets = new ArrayList();
                for(JavaType item: elements) {
                    if(item.getRefrences().isEmpty()&&!item.isRelevant()) {
                        Workspace workspace = (Workspace)app.getWorkspaceComponent();
                        workspace.getCanvas().getChildren().remove(item.getRight());
                        workspace.getCanvas().getChildren().remove(item.getBox());
                        targets.add(item);
                    }
                }
                for(JavaType item : targets) {
                    elements.remove(item);
                }
                return;
            }
            for(JavaType item : elements) {
                if(item.getName().equals(name)) {
                    selected.setParentJT(item);
                    item.addRef(selected);
                    return;
                }
            }
            JavaType ref = addExternalSpl(name);
            if(ref!=null) {
                selected.setParentJT(ref);
                ref.addRef(selected);
            }
        }
    }
    
    //VARIABLE DATA
    public JavaType addExternal(String type) {
        switch(type) {
            case "int":
            case "float":
            case "double":
            case "char":
            case "short":
            case "byte":
            case "long":
            case "boolean":
            case "void":
                return null;
            default:
                for(JavaType item : elements) {
                    if(item.getName().equals(type)) {
                        return null;
                    }
                }
        }
        CanvasController canvasController = new CanvasController(app);
        return canvasController.addClass(100, 100, type,false);
    }
    
    public JavaType addExternalSpl(String type) {
        switch(type) {
            case "int":
            case "float":
            case "double":
            case "char":
            case "short":
            case "byte":
            case "long":
            case "boolean":
            case "void":
                return null;
            default:
                for(JavaType item : elements) {
                    if(item.getName().equals(type)) {
                        return item;
                    }
                }
        }
        CanvasController canvasController = new CanvasController(app);
        return canvasController.addClass(100, 100, type,false);
    }
    
    //METHOD DATA
    
    //MISC
    /**
     * This function clears out the HTML tree and reloads it with the minimal
     * tags, like html, head, and body such that the user can begin editing a
     * page.
     */
    @Override
    public void reset() {
        elements = new ArrayList<>();
        pkgNames = new ArrayList<>();
        snapMode = false;
        selected = null;
        ClassJT.reset();
        InterfaceJT.reset();
        app.getWorkspaceComponent().reloadWorkspace();
    }
    
    public void renderLines() {
        for(JavaType element : elements) {
            for(JavaType item: element.getRefrences()) {
                Line line = new Line();
                line.setStrokeWidth(2);
                line.startXProperty().bind(element.getBox().layoutXProperty());
                line.startYProperty().bind(element.getBox().layoutYProperty());
                line.endXProperty().bind(item.getBox().layoutXProperty());
                line.endYProperty().bind(item.getBox().layoutYProperty());
                Workspace workspace = (Workspace) app.getWorkspaceComponent();
                workspace.getCanvas().getChildren().add(line);
            }
        }
    }
    
    //Reload workspace to show updated information
    public void updateGUI() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace();
    }
    
    public boolean isReadyForExport() {
        for(JavaType element : elements) {
            if(!element.getValidity()) {
                return false;
            }
        }
        return true;
    }
}
