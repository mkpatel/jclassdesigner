/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Alert;
import javafx.scene.effect.Effect;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Window;
import javax.imageio.ImageIO;
import jcd.data.AppState;
import jcd.data.DataManager;
import jcd.data.JavaType;
import jcd.gui.Workspace;
import saf.AppTemplate;

/**
 *
 * @author Mrunal
 */
public class AppController {
    AppTemplate app;
    
    DataManager dataManager;
    /*
    Init the components related to the app when making the element controller;
    */
    public AppController(AppTemplate initApp) {
        app = initApp;
        dataManager = (DataManager) app.getDataComponent();
    }
    
    public void addMode() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();

        dataManager.setState(AppState.ADD_MODE);
        //update buttons method
        workspace.updateButtonStat();
    }
    
    public void selectMode() {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        
        dataManager.setState(AppState.SELECT_MODE);
        //update buttons method
        workspace.updateButtonStat();
    }
    
    public void exportCode(Window ownerWindow) {
        if(dataManager.isReadyForExport()) {
            DirectoryChooser dirChooser = new DirectoryChooser();
            dirChooser.setTitle("Export Code");
            File file = dirChooser.showDialog(ownerWindow);
            ArrayList<String> pkgs = dataManager.getUniquePkgs();
            for(String currentpkg : pkgs) {
                ArrayList<JavaType> inPkg = dataManager.findByPkg(currentpkg);
                currentpkg = currentpkg.replace('.', '\\');
                File folder = new File(file.getPath()+"\\src\\"+currentpkg);
                folder.mkdirs();
                for(JavaType element : inPkg) {
                    if(element.isRelevant()) {
                        File jFile = new File(folder,element.getName()+".java");
                        try {
                            PrintWriter writer = new PrintWriter(jFile.getPath(), "UTF-8");
                            writer.print(element.exportToCode());
                            writer.close();
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(AppController.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (UnsupportedEncodingException ex) {
                            Logger.getLogger(AppController.class.getName()).log(Level.SEVERE, null, ex);
                        }  
                    }
                }
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error!");
            alert.setHeaderText("Please fix the errors in the diagram!");
            alert.setContentText("You're not allowed to have the same class name multiple times"
                    + "in the same package!");
            alert.showAndWait();
        }
        
    }
    
    public void exportPhoto(Window ownerWindow, Pane canvas) {
        JavaType selected = null;
        Effect toRestore = null;
        double scalex = canvas.getScaleX();
        double scaley = canvas.getScaleY();
        canvas.setScaleX(1);
        canvas.setScaleY(1);
        if(dataManager.isInState(AppState.SELECT_MODE)) {
            selected = dataManager.getSelected();
            if(selected != null) {
                toRestore = selected.getBox().getEffect();
                selected.getBox().setEffect(null);
            }
        }
        DirectoryChooser dirChooser = new DirectoryChooser();
        dirChooser.setTitle("Save Image");
        File dir = dirChooser.showDialog(ownerWindow);
        File file = new File(dir.getPath()+"\\uml.png");
        WritableImage image = canvas.snapshot(new SnapshotParameters(), null);
        try {
	    ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
	}
	catch(IOException ioe) {
	    ioe.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error!");
            alert.setHeaderText("Could not save snapshot!");
            alert.setContentText(null);
            alert.showAndWait();
        } finally {
            if(selected != null) {
                selected.getBox().setEffect(toRestore);
            }
            canvas.setScaleX(scalex);
            canvas.setScaleY(scaley);
            return;
        }
        
    }
    
}
