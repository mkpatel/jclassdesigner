/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.controller;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import jcd.data.DataManager;
import jcd.data.InterfaceJT;
import jcd.data.JavaType;
import jcd.data.MethodJT;
import jcd.data.VariableJT;

/**
 *
 * @author Mrunal
 */
public class MethodDialog extends Stage {
    
    DataManager dm;
    //MAIN COMPONENTS
    TableView methodTable;
    JavaType selected;
    MethodJT method;
    
    // HERE ARE THE DIALOG COMPONENTS
    VBox messagePane;
    Scene messageScene;
    Button closeButton;
    Button actionButton;
    Button addButton;
//    TextField paramName;
//    TextField paramType;
    TextField name;
    TextField type;
    CheckBox isStatic;
    CheckBox isAbstract;
    ChoiceBox visibility;
    VBox params;
    
    int numParams;
    String msg;
    boolean edit;
    
    public MethodDialog(Stage owner,TableView table, JavaType element, String type, DataManager dm) {
        this.dm = dm;
        msg = type;
        if(msg.equals("Edit")) {
            edit = true;
        } else {
            edit = false;
        }
        methodTable = table;
        selected = element;
        init(owner);
        if(element instanceof InterfaceJT) {
           isAbstract.setSelected(true);
           isAbstract.setDisable(true);
        }
    }
    
    /**
     * This function fully initializes the singleton dialog for use.
     * 
     * @param owner The window above which this dialog will be centered.
     */
    public void init(Stage owner) {
        // MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);      

        HBox buttonBox = new HBox();
        
        // CLOSE BUTTON
        closeButton = new Button("Close");
        closeButton.setOnAction(e->{ MethodDialog.this.close(); });
        
        // ADD BUTTON
        addButton = new Button("Add Param");
        addButton.setOnAction(e->{
            TextField paramName = new TextField();
            TextField paramType = new TextField();
            HBox element = new HBox();
            element.getChildren().addAll(paramName,paramType);
            params.getChildren().add(element);
            numParams++;
        });
        
        // ACTION BUTTON
        actionButton = new Button(msg);
        actionButton.setOnAction(e->{
            String name = this.name.getText();
            String type = this.type.getText();
            if(name.equals("")||type.equals("")) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error!");
                alert.setHeaderText("Invalid Method!");
                alert.setContentText("Some of the fields entered were invalid.");
                alert.showAndWait();
            } else {
                if(edit) {
                   selected.removeMethod(method);
                }
                method.setName(name);
                method.setType(type);
                method.setVisibility(visibility.getSelectionModel().getSelectedItem().toString());
                method.setAbstract(isAbstract.isSelected());
                method.setStatic(isStatic.isSelected());
                boolean fail = false;
                ArrayList<VariableJT> paramlist = new ArrayList();
                for(int i = 0; i < numParams; i++) {
                    String pname = ((TextField)((HBox)params.getChildren().get(i)).getChildren().get(0)).getText();
                    String ptype = ((TextField)((HBox)params.getChildren().get(i)).getChildren().get(1)).getText();
                    if(pname.equals("")&&!ptype.equals("")||!pname.equals("")&&ptype.equals("")) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error!");
                        alert.setHeaderText("Invalid Method!");
                        alert.setContentText("Some of the fields entered were invalid.");
                        alert.showAndWait();
                        fail = true;
                        break;
                    }
                    if(!pname.equals("")&&!ptype.equals("")) {
                        paramlist.add(new VariableJT(pname,ptype));
                        JavaType ref = dm.addExternal(ptype);
                        if(ref!=null) {
                            ref.addRef(selected);
                        }
                    }
                }
                if(!fail) {
                    method.setParams(paramlist);
                    selected.addMethod(method);
                    methodTable.setItems(selected.getMethodsData());
                    JavaType ref = dm.addExternal(type);
                    if(ref!=null) {
                        ref.addRef(selected);
                    }
                    MethodDialog.this.close();
                }
            }
            
        });
        
        buttonBox.getChildren().addAll(actionButton,closeButton);
        buttonBox.setAlignment(Pos.BOTTOM_RIGHT);
        buttonBox.setSpacing(5);
        //OPTIONS
        Label nameLabel = new Label("Name ");
        Label typeLabel = new Label("Type  ");
        Label staticLabel = new Label("Static ");
        Label abstractLabel = new Label("Abstract ");
        Label visibilityLabel = new Label("Access ");
        HBox nameBox = new HBox();
        HBox typeBox = new HBox();
        HBox staticBox = new HBox();
        HBox visibilityBox = new HBox();
        HBox abstractBox = new HBox();
        name = new TextField();
        type = new TextField();
        isStatic = new CheckBox();
        isAbstract = new CheckBox();
        visibility = new ChoiceBox(FXCollections.observableArrayList("default","public","private","protected"));
        visibility.getSelectionModel().selectFirst();
        params = new VBox();
        Label paramLabel = new Label("Params (Name,Type)");
        // WE'LL PUT EVERYTHING HERE
        nameBox.getChildren().addAll(nameLabel,name);
        typeBox.getChildren().addAll(typeLabel,type);
        staticBox.getChildren().addAll(staticLabel,isStatic);
        visibilityBox.getChildren().addAll(visibilityLabel,visibility);
        abstractBox.getChildren().addAll(abstractLabel,isAbstract);
        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().addAll(nameBox,typeBox,visibilityBox,staticBox,abstractBox,paramLabel,params,addButton,buttonBox);
        
        // MAKE IT LOOK NICE
        messagePane.setPadding(new Insets(80, 60, 80, 60));
        messagePane.setSpacing(20);

        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);
    }
 
    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     * 
     * @param title The title to appear in the dialog window.
     * 
     * @param message Message to appear inside the dialog.
     */
    public void show(String title, MethodJT method) {
	// SET THE DIALOG TITLE BAR TITLE
	setTitle(title);
	
	// SET THE MESSAGE TO DISPLAY TO THE USER
        this.method = method;
	
        name.setText(method.getName());
        type.setText(method.getType());
        isStatic.setSelected(method.isIsStatic());
        isAbstract.setSelected(method.isIsAbstract());
        visibility.getSelectionModel().select(method.getVisibility());
        
        numParams = method.getParams().size();
        for(int i = 0 ; i < numParams; i++) {
            TextField paramName = new TextField();
            TextField paramType = new TextField();
            HBox element = new HBox();
            VariableJT var = method.getParams().get(i);
            paramName.setText(var.getName());
            paramType.setText(var.getType());
            element.getChildren().addAll(paramName,paramType);
            params.getChildren().add(element);
        }
        TextField paramName = new TextField();
        TextField paramType = new TextField();
        HBox element = new HBox();
        element.getChildren().addAll(paramName,paramType);
        params.getChildren().add(element);
        numParams++;
        
	// AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
	// WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
	// DO MORE WORK.
        showAndWait();
    }
}
