/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import jcd.data.AppState;
import jcd.data.DataManager;
import jcd.data.InterfaceJT;
import jcd.data.JavaType;
import jcd.data.MethodJT;
import jcd.data.VariableJT;
import saf.AppTemplate;

/**
 *
 * @author Mrunal
 */
public class ElementController {
    
    AppTemplate app;
    DataManager dataManager;
    /*
    Init the components related to the app when making the element controller;
    */
    public ElementController(AppTemplate initApp) {
        app = initApp;
        dataManager = (DataManager) app.getDataComponent();
    }
    
    public void moveClass(double x, double y) {
        if(dataManager.isInState(AppState.SELECT_MODE)) {
            JavaType current = dataManager.getSelected();
            if(current!=null) {
                if(dataManager.getSnapMode()) {
                    x = 25*(Math.round(x/25));
                    y = 25*(Math.round(y/25));                
                }
                current.setX(x);
                current.setY(y);
            }
        }        
    }
    
    public void selectClass(VBox box) {
        if(dataManager.isInState(AppState.SELECT_MODE)) {
            JavaType current = dataManager.find(box);
            dataManager.setSelected(current);
            current.selectEffectOn();
        }
    }
    
    public void updateName(String newVal) {
        if(dataManager.isInState(AppState.SELECT_MODE)) {
            JavaType selected = dataManager.getSelected();
            if(selected!=null) {
                selected.setName(newVal);
                boolean valid = dataManager.isValidJavaType();
                selected.setValidity(valid);
                if(!valid) {
                    //create alert here
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("Warning!");
                    alert.setHeaderText("This code will not generate!");
                    alert.setContentText("You're not allowed to have the same class name"
                            + " multiple times in the same package.");
                    alert.showAndWait();
                }
            }
        }
    }
    
    public void updatePkg(String newVal) {
        if(dataManager.isInState(AppState.SELECT_MODE)) {
            JavaType selected = dataManager.getSelected();
            if(selected!=null) {
                dataManager.setPkg(newVal);
                boolean valid = dataManager.isValidJavaType();
                selected.setValidity(valid);
                if(!valid) {
                    //create alert here
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("Warning!");
                    alert.setHeaderText("This code will not generate!");
                    alert.setContentText("You're not allowed to have the same class name"
                            + " multiple times in the same package.");
                    alert.showAndWait();
                }
            }
        }
    }

    public void addInterface() {
        if (dataManager.isInState(AppState.SELECT_MODE)) {
            ArrayList<String> applicationInterfaces = dataManager.getInterfaceNames();
            ArrayList<String> elementInterfaces = dataManager.getSelected().getInterfaceNames();
            ArrayList<String> dialogData = new ArrayList();
            dialogData.add("Custom...");
            for(String item : applicationInterfaces) {
                if(!elementInterfaces.contains(item)) {
                    dialogData.add(item);
                }
            }
            ChoiceDialog dialog = new ChoiceDialog(dialogData.get(0), dialogData);
            dialog.setTitle("Add interface");
            dialog.setHeaderText("Select your interface");
            Optional<String> result = dialog.showAndWait();
            String choice = null;
            if (result.isPresent()) {
                choice = result.get();
            }
            if (choice != null) {
                if(choice.equals("Custom...")) {
                    TextInputDialog customDialog = new TextInputDialog("");
                    customDialog.setTitle("Custom Interface");
                    customDialog.setHeaderText("This dialog can be used to enter a standard interface.");
                    customDialog.setContentText("Interface name: ");
                    Optional<String> customInterface = customDialog.showAndWait();
                    customInterface.ifPresent(value -> {
                        CanvasController canvasController = new CanvasController(app);
                        InterfaceJT customInterfaceObject = canvasController.addInterface(100, 100, value,false);
                        dataManager.addInterface(customInterfaceObject.getName());
                    });
                } else {
                    dataManager.addInterface(choice);
                }
            }
            
        }
    }
    
    public void removeInterface() {
        if (dataManager.isInState(AppState.SELECT_MODE)) {
            JavaType selected = dataManager.getSelected();
            if (selected != null) {
                List<String> dialogData = selected.getInterfaceNames();
                if(dialogData.size() > 0) {
                    ChoiceDialog dialog = new ChoiceDialog(dialogData.get(0), dialogData);
                    dialog.setTitle("Remove interface");
                    dialog.setHeaderText("Select your interface");
                    Optional<String> result = dialog.showAndWait();
                    String choice = null;
                    if (result.isPresent()) {
                        choice = result.get();
                    }
                    if (choice != null) {
                        dataManager.removeInterface(choice);
                    }
                } else {
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("Warning!");
                    alert.setHeaderText("No Interfaces Implemented!");
                    alert.setContentText("There are no more interfaces left to remove.");
                    alert.showAndWait();
                }
            } 
        }
    }
    
    public void addDep() {
        if(dataManager.isInState(AppState.SELECT_MODE)) {
            JavaType selected = dataManager.getSelected();
            if(selected != null) {
                TextInputDialog customDialog = new TextInputDialog("");
                customDialog.setTitle("Add Dependency");
                customDialog.setHeaderText("This dialog can be used to add a dependency (package).");
                customDialog.setContentText("Package name: ");
                Optional<String> customInterface = customDialog.showAndWait();
                customInterface.ifPresent(value -> {
                    selected.addDependency(value);
                    app.getWorkspaceComponent().reloadWorkspace();
                });
            } 
        }     
    }
    
    public void removeDep() {
        if(dataManager.isInState(AppState.SELECT_MODE)) {
            JavaType selected = dataManager.getSelected();
            if(selected != null) {
                List<String> dialogData = selected.getDependencies();
                if(dialogData.size() > 0) {
                    ChoiceDialog dialog = new ChoiceDialog(dialogData.get(0), dialogData);
                    dialog.setTitle("Remove Dependency");
                    dialog.setHeaderText("Select the package you wish to remove");
                    Optional<String> result = dialog.showAndWait();
                    String choice = null;
                    if (result.isPresent()) {
                        choice = result.get();
                    }
                    if (choice != null) {
                        selected.removeDependency(choice);
                        app.getWorkspaceComponent().reloadWorkspace();
                    }
                } else {
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("Warning!");
                    alert.setHeaderText("No Dependencies Present!");
                    alert.setContentText("There are no more dependencies left to remove.");
                    alert.showAndWait();
                }
            } 
        }
    }
    
    public void setParent(String name) {
        if (dataManager.isInState(AppState.SELECT_MODE)) {
            dataManager.setParent(name);
        }  
    }

    //TODO
    public void addVariable(TableView varTable, Stage owner) {
        if(dataManager.isInState(AppState.SELECT_MODE)) {
            JavaType selected = dataManager.getSelected();
            VariableDialog dialog = new VariableDialog(owner,varTable,selected,"Add",dataManager);
            dialog.show("New Variable", new VariableJT("String","name","default",true,false));                               
        }
    }
    
    //TODO
    public void removeVariable(TableView varTable) {
        if(dataManager.isInState(AppState.SELECT_MODE)) {
            JavaType selected = dataManager.getSelected();
            List<String> dialogData = selected.getVarNames();
            if(dialogData.size() > 0) {
                ChoiceDialog dialog = new ChoiceDialog(dialogData.get(0), dialogData);
                dialog.setTitle("Remove variable");
                dialog.setHeaderText("Select the variable you wish to remove");
                Optional<String> result = dialog.showAndWait();
                String choice = null;
                if (result.isPresent()) {
                    choice = result.get();
                }
                if (choice != null) {
                    VariableJT var = selected.findVariable(choice);
                    selected.removeVariable(var);
                    varTable.getItems().remove(var);
                }
            } else {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Warning!");
                alert.setHeaderText("No Variables Remaining!");
                alert.setContentText("There are no more variables left to remove.");
                alert.showAndWait();
            }
        }
    }
    
    public void addMethod(TableView methodTable, Stage owner) {
        if(dataManager.isInState(AppState.SELECT_MODE)) {
            JavaType selected = dataManager.getSelected();
            MethodDialog dialog = new MethodDialog(owner,methodTable,selected,"Add",dataManager);
            dialog.show("New Method", new MethodJT());                               
        }
    }
    
    public void removeMethod(TableView methodTable) {
        if(dataManager.isInState(AppState.SELECT_MODE)) {
            JavaType selected = dataManager.getSelected();
            List<String> dialogData = selected.getMethodNames();
            if(dialogData.size() > 0) {
                ChoiceDialog dialog = new ChoiceDialog(dialogData.get(0), dialogData);
                dialog.setTitle("Remove method");
                dialog.setHeaderText("Select the method you wish to remove");
                Optional<String> result = dialog.showAndWait();
                String choice = null;
                if (result.isPresent()) {
                    choice = result.get();
                }
                if (choice != null) {
                    MethodJT method = selected.findMethod(choice);
                    selected.removeMethod(method);
                    methodTable.getItems().remove(method);
                }
                app.getWorkspaceComponent().reloadWorkspace();
            } else {
                Alert alert = new Alert(AlertType.WARNING);
                alert.setTitle("Warning!");
                alert.setHeaderText("No Methods Remaining!");
                alert.setContentText("There are no more methods left to remove.");
                alert.showAndWait();
            }
        }
    }
}