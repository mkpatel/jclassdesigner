/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.controller;

import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import jcd.data.AppState;
import jcd.data.JavaType;
import jcd.data.DataManager;
import jcd.data.ClassJT;
import jcd.data.InterfaceJT;
import jcd.gui.Workspace;
import saf.AppTemplate;

/**
 *
 * @author Mrunal
 */
public class CanvasController {
    
    final int xDisp = 460;
    final int yDisp = 80;
    
    AppTemplate app;
    DataManager dataManager;
    ElementController elementController;
    /*
    Init the components related to the app when making the element controller;
    */
    public CanvasController(AppTemplate initApp) {
        app = initApp;
        dataManager = (DataManager) app.getDataComponent();
        elementController = new ElementController(app);
    }
    
    public ClassJT addClass(double x, double y, String n, boolean relevant) {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        ClassJT newClass = new ClassJT(x,y,n,relevant);
        newClass.getBox().setOnMouseClicked(e -> {
            for(int i = 0; i < canvas.getChildren().size(); i++) {
                Node node = canvas.getChildren().get(i);
                if(node instanceof VBox) {
                    VBox box = (VBox) node;
                    JavaType element = dataManager.find(box);
                    element.selectEffectOff();
                }
            }
            elementController.selectClass((VBox)e.getSource());
        });
        newClass.getBox().setOnMouseDragged(e -> {
            elementController.moveClass(e.getSceneX()-xDisp, e.getSceneY()-yDisp);
        });
        newClass.getBox().setOnMouseDragReleased(e->{
            app.getWorkspaceComponent().reloadWorkspace();
        });
        canvas.getChildren().add(newClass.getRight());
        dataManager.addElement(newClass);
        return newClass;
    }
    
    public InterfaceJT addInterface(double x, double y, String n, boolean relevant) {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        InterfaceJT newInterface = new InterfaceJT(x,y,n,relevant);
        newInterface.getBox().setOnMouseClicked(e -> {
            for(int i = 0; i < canvas.getChildren().size(); i++) {
                Node node = canvas.getChildren().get(i);
                if(node instanceof VBox) {
                    VBox box = (VBox) node;
                    JavaType element = dataManager.find(box);
                    element.selectEffectOff();
                }
            }
            elementController.selectClass((VBox)e.getSource());
        });
        newInterface.getBox().setOnMouseDragged(e -> {
            elementController.moveClass(e.getSceneX()-xDisp, e.getSceneY()-yDisp);
        });
        newInterface.getBox().setOnMouseDragReleased(e->{
            app.getWorkspaceComponent().reloadWorkspace();
        });
        canvas.getChildren().add(newInterface.getRight());
        dataManager.addElement(newInterface);
        return newInterface;
    }
    
    public void removeJT() {
        if(dataManager.isInState(AppState.SELECT_MODE)) {
            dataManager.removeElement();
        }
    }
    
}
