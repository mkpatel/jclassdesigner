/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.controller;

import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import jcd.data.DataManager;
import jcd.data.InterfaceJT;
import jcd.data.JavaType;
import jcd.data.VariableJT;

/**
 *
 * @author Mrunal
 */
public class VariableDialog extends Stage {
    
    DataManager dm;
    
    //MAIN COMPONENTS
    TableView varTable;
    JavaType selected;
    VariableJT variable;
    
    // HERE ARE THE DIALOG COMPONENTS
    VBox messagePane;
    Scene messageScene;
    Button closeButton;
    Button actionButton;
    TextField name;
    TextField type;
    CheckBox isStatic;
    CheckBox isFinal;
    ChoiceBox visibility;
    
    String msg;
    boolean edit;
    
    public VariableDialog(Stage owner,TableView table, JavaType element, String type, DataManager dm) {
        this.dm = dm;
        msg = type;
        if(msg.equals("Edit")) {
            edit = true;
        } else {
            edit = false;
        }
        varTable = table;
        selected = element;
        init(owner);
        if(element instanceof InterfaceJT) {
           isStatic.setSelected(true);
           isFinal.setSelected(true);
           isStatic.setDisable(true);
           isFinal.setDisable(true);
        }
    }
    
    /**
     * This function fully initializes the singleton dialog for use.
     * 
     * @param owner The window above which this dialog will be centered.
     */
    public void init(Stage owner) {
        // MAKE IT MODAL
        initModality(Modality.WINDOW_MODAL);
        initOwner(owner);      

        HBox buttonBox = new HBox();
        
        // CLOSE BUTTON
        closeButton = new Button("Close");
        closeButton.setOnAction(e->{ VariableDialog.this.close(); });
        
        // ACTION BUTTON
        actionButton = new Button(msg);
        actionButton.setOnAction(e->{
            String name = this.name.getText();
            String type = this.type.getText();
            if(name.equals("")||type.equals("")) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error!");
                alert.setHeaderText("Invalid Variable!");
                alert.setContentText("Some of the fields entered were invalid.");
                alert.showAndWait();
            } else {
                if(edit) {
                   selected.removeVariable(variable);
                }
                if(selected.validVariable(name)) {
                    variable.setName(name);
                    variable.setType(type);
                    variable.setVisibility(visibility.getSelectionModel().getSelectedItem().toString());
                    variable.setFinal(isFinal.isSelected());
                    variable.setStatic(isStatic.isSelected());
                    selected.addVariable(variable);
                    varTable.setItems(selected.getVarsData());
                    JavaType ref = dm.addExternal(type);
                    if(ref!=null) {
                        ref.addRef(selected);
                    }
                    VariableDialog.this.close();
                } else {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Warning!");
                    alert.setHeaderText("Duplicate Variable!");
                    alert.setContentText("You cannot have more than one variable with the same name.");
                    alert.showAndWait();
                }
            }
            
        });
        
        buttonBox.getChildren().addAll(actionButton,closeButton);
        buttonBox.setAlignment(Pos.BOTTOM_RIGHT);
        buttonBox.setSpacing(5);
        //OPTIONS
        Label nameLabel = new Label("Name ");
        Label typeLabel = new Label("Type  ");
        Label staticLabel = new Label("Static ");
        Label finalLabel = new Label("Final ");
        Label visibilityLabel = new Label("Access ");
        HBox nameBox = new HBox();
        HBox typeBox = new HBox();
        HBox staticBox = new HBox();
        HBox visibilityBox = new HBox();
        HBox finalBox = new HBox();
        name = new TextField();
        type = new TextField();
        isStatic = new CheckBox();
        isFinal = new CheckBox();
        visibility = new ChoiceBox(FXCollections.observableArrayList("default","public","private","protected"));
        visibility.getSelectionModel().selectFirst();

        // WE'LL PUT EVERYTHING HERE
        nameBox.getChildren().addAll(nameLabel,name);
        typeBox.getChildren().addAll(typeLabel,type);
        staticBox.getChildren().addAll(staticLabel,isStatic);
        visibilityBox.getChildren().addAll(visibilityLabel,visibility);
        finalBox.getChildren().addAll(finalLabel,isFinal);
        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().addAll(nameBox,typeBox,visibilityBox,staticBox,finalBox,buttonBox);
        
        // MAKE IT LOOK NICE
        messagePane.setPadding(new Insets(80, 60, 80, 60));
        messagePane.setSpacing(20);

        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);
    }
 
    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     * 
     * @param title The title to appear in the dialog window.
     * 
     * @param message Message to appear inside the dialog.
     */
    public void show(String title, VariableJT var) {
	// SET THE DIALOG TITLE BAR TITLE
	setTitle(title);
	
	// SET THE MESSAGE TO DISPLAY TO THE USER
        variable = var;
	
        name.setText(var.getName());
        type.setText(var.getType());
        isStatic.setSelected(var.isIsStatic());
        isFinal.setSelected(var.isIsFinal());
        visibility.getSelectionModel().select(var.getVisibility());
        
	// AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
	// WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
	// DO MORE WORK.
        showAndWait();
    }
}
