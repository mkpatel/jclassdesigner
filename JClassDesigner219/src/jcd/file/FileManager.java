/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import jcd.data.ClassJT;
import jcd.data.DataManager;
import jcd.data.JavaType;
import jcd.data.MethodJT;
import jcd.data.VariableJT;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 *
 * @author Mrunal
 */
public class FileManager implements AppFileComponent {
    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        //GET THE DATA
        DataManager dataManager = (DataManager) data;
        
        JsonArrayBuilder elementArrayBuilder = Json.createArrayBuilder();
        ArrayList<JavaType> elements = dataManager.getElements();
        for(JavaType element : elements) {
            //STORE CLASS INFORMATION HERE
            String type = "";
            if(element instanceof ClassJT) {
                type = "CLASS";
            } else {
                type = "INTERFACE";
            }
            String xcor = Double.toString(element.getX());
            String ycor = Double.toString(element.getY());
            String pkg = element.getPkg();
            boolean isAbstract = element.getAbstract();
            String name = element.getName();            
            String parent = element.getParent();
            ArrayList<String> deps = element.getDependencies();
            JsonArray depsJson = makeDependencyJson(deps);
            ArrayList<String> interfaces = element.getInterfaceNames();
            JsonArray interfaceJson = makeInterfaceJson(interfaces);
            ArrayList<VariableJT> vars = element.getVars();
            JsonArray varJson = makeVarJson(vars);
            ArrayList<MethodJT> methods = element.getMethods();
            JsonArray methodJson = makeMethodJson(methods);
            JsonObject elementJson = Json.createObjectBuilder()
                    .add("type",type)
                    .add("xcor",xcor)
                    .add("ycor",ycor)
                    .add("package", pkg)
                    .add("abstract", isAbstract)
                    .add("name", name)
                    .add("dependencies",depsJson)
                    .add("parent", parent)
                    .add("interfaces", interfaceJson)
                    .add("variables", varJson)
                    .add("methods", methodJson).build();
            elementArrayBuilder.add(elementJson);
        }
        
        // THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonArray elementArray = elementArrayBuilder.build();
        JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add("elements", elementArray).build(); 
        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    private JsonArray makeVarJson(ArrayList<VariableJT> vars) {
        JsonArrayBuilder varArray = Json.createArrayBuilder();
        for(VariableJT v : vars) {
            String type = v.getType();
            String name = v.getName();
            String visibility = v.getVisibility();
            boolean isStatic = v.isIsStatic();
            boolean isFinal = v.isIsFinal();
            JsonObject varJson = Json.createObjectBuilder()
                    .add("type", type)
                    .add("name", name)
                    .add("visibility", visibility)
                    .add("static", isStatic)
                    .add("final", isFinal).build();
            varArray.add(varJson);
        }
        return varArray.build();
    }
    
    private JsonArray makeMethodJson(ArrayList<MethodJT> methods) {
        JsonArrayBuilder methodArray = Json.createArrayBuilder();
        for(MethodJT m : methods) {
            String type = m.getType();
            String name = m.getName();
            String visibility = m.getVisibility();
            boolean isStatic = m.isIsStatic();
            boolean isAbstract = m.isIsAbstract();
            ArrayList<VariableJT> params = m.getParams();
            JsonArray paramsJson = makeParamJson(params);
            JsonObject methodJson = Json.createObjectBuilder()
                    .add("type", type)
                    .add("name", name)
                    .add("visibility", visibility)
                    .add("static", isStatic)
                    .add("abstract", isAbstract)
                    .add("params", paramsJson).build();
            methodArray.add(methodJson);
        }
        return methodArray.build();
    }
    
    private JsonArray makeParamJson(ArrayList<VariableJT> vars) {
        JsonArrayBuilder varArray = Json.createArrayBuilder();
        for(VariableJT v : vars) {
            String type = v.getType();
            String name = v.getName();
            JsonObject varJson = Json.createObjectBuilder()
                    .add("type", type)
                    .add("name", name).build();
            varArray.add(varJson);
        }
        return varArray.build();
    }
    
    private JsonArray makeInterfaceJson(ArrayList<String> interfaces) {
        JsonArrayBuilder interfaceArray = Json.createArrayBuilder();
        for(String s : interfaces) {
            interfaceArray.add(s);
        }
        return interfaceArray.build();
    }
    
    private JsonArray makeDependencyJson(ArrayList<String> deps) {
        JsonArrayBuilder depArray = Json.createArrayBuilder();
        for(String s : deps) {
            depArray.add(s);
        }
        return depArray.build();
    }
    
    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param data Data management component where we'll load the file into.
     * 
     * @param filePath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error reading
     * in data from the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        JsonObject sessionObject = loadJSONFile(filePath);
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    /**
     * This method exports the contents of the data manager to a 
     * Web page including the html page, needed directories, and
     * the CSS file.
     * 
     * @param data The data management component.
     * 
     * @param filePath Path (including file name/extension) to where
     * to export the page to.
     * 
     * @throws IOException Thrown should there be an error writing
     * out data to the file.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {

    }
    
    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
	// NOTE THAT THE Web Page Maker APPLICATION MAKES
	// NO USE OF THIS METHOD SINCE IT NEVER IMPORTS
	// EXPORTED WEB PAGES
    }
}
