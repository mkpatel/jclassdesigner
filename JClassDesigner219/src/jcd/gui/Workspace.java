/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jcd.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import jcd.controller.AppController;
import jcd.controller.CanvasController;
import jcd.controller.ElementController;
import jcd.data.AppState;
import jcd.data.DataManager;
import jcd.data.JavaType;
import properties_manager.PropertiesManager;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import static jcd.PropertyType.*;
import jcd.controller.MethodDialog;
import jcd.controller.VariableDialog;
import jcd.data.MethodJT;
import jcd.data.VariableJT;
import static saf.settings.AppStartupConstants.FILE_PROTOCOL;
import static saf.settings.AppStartupConstants.PATH_IMAGES;
/**
 *
 * @author Mrunal
 */
public class Workspace extends AppWorkspaceComponent {
      // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;
    
    DataManager dataManager;
    
    AppController appController;
    CanvasController canvasController;
    ElementController elementController;
    
    //TOOLBAR
    ToolBar drawControls;
    Button select;
    Button resize;
    Button classB;
    Button interfaceB;
    Button remove;
    Button undo;
    Button redo;
    ToolBar viewControls;
    Button zoomIn;
    Button zoomOut;
    CheckBox grid;
    CheckBox snap;
    ToolBar exportControls;
    Button snapshot;
    Button code;
    HBox spaceBox;
    HBox spaceBox2;
    HBox spaceBox3;
    
    //LEFT PANE
    TabPane left;
    Tab classTab;
    Tab vmTab;
    VBox vminfo;
    VBox classinfo;
    HBox classBox;
    Label className;
    TextField classText;
    HBox pkgBox;
    Label pkgName;
    TextField pkgText;
    HBox parentBox;
    Label parentName;
    ChoiceBox parentChoice;
    HBox interfaceHBox;
    VBox interfaceVBox;
    Label interfaceName;
    Button addInterface;
    Button removeInterface;
    ScrollPane interfaceScroll;
    VBox interfaceList;
    HBox depHBox;
    VBox depVBox;
    Label depName;
    Button addDep;
    Button removeDep;
    ScrollPane depScroll;
    VBox depList;
    HBox varBox;
    Label varName;
    Button addVar;
    Button removeVar;
    ScrollPane varTableBox;
    TableView varTable;
    TableColumn<VariableJT,String> varNameCol;
    TableColumn<VariableJT,String> varTypeCol;
    TableColumn<VariableJT,Boolean> varStaticCol;
    TableColumn<VariableJT,Boolean> varFinalCol;
    TableColumn<VariableJT,String> varAccessCol;
    HBox methodBox;
    Label methodName;
    Button addMethod;
    Button removeMethod;
    ScrollPane methodTableBox;
    TableView methodTable;
    TableColumn<MethodJT, String> methodNameCol;
    TableColumn<MethodJT, String> methodReturnCol;
    TableColumn<MethodJT, Boolean> methodStaticCol;
    TableColumn<MethodJT, Boolean> methodAbstractCol;
    TableColumn<MethodJT, String> methodAccessCol;
    
    //CANVAS
    ScrollPane center;
    Pane canvas;
    
    public static final String CLASS_LEFTBOX = "jcd_leftbox";
    public static final String CLASS_EDITBOX = "jcd_editbox";
    public static final String CLASS_CANVAS = "jcd_canvas";
    public static final String CLASS_LABEL = "jcd_label";
    public static final String CLASS_H1 = "jcd_h1";
    public static final String CLASS_H2 = "jcd_h2";
    public static final String CLASS_FILE_BUTTON_SELECT = "file_button_select";
    public static final String CLASS_FILE_BUTTON_HOVER = "file_button_hover";
    
    private boolean addingClass;
    private double zoom = 1;
    private ArrayList<Line> gridlines;
    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
	// KEEP THIS FOR LATER
	app = initApp;
        
        dataManager = (DataManager) app.getDataComponent();
        
	// KEEP THE GUI FOR LATER
	gui = app.getGUI();
        initializeGUI(gui.getAppPane());
        initializeHandlers();
    }
    
    private void initializeGUI(BorderPane appPane) {

        select = initChildButton(SELECT_ICON.toString(), SELECT_TOOLTIP.toString(), false);
        resize = initChildButton(RESIZE_ICON.toString(), RESIZE_TOOLTIP.toString(), false);
        classB = initChildButton(CLASS_ICON.toString(), CLASS_TOOLTIP.toString(), false);
        interfaceB = initChildButton(INTERFACE_ICON.toString(), INTERFACE_TOOLTIP.toString(), false);
        remove = initChildButton(DELETE_ICON.toString(), DELETE_TOOLTIP.toString(), false);
        undo = initChildButton(UNDO_ICON.toString(), UNDO_TOOLTIP.toString(), false);
        redo = initChildButton(REDO_ICON.toString(), REDO_TOOLTIP.toString(), false);
        drawControls = new ToolBar(select, resize, classB, interfaceB, remove, undo, redo);
        
        zoomIn = initChildButton(ZOOM_IN_ICON.toString(), ZOOM_IN_TOOLTIP.toString(), false);
        zoomOut = initChildButton(ZOOM_OUT_ICON.toString(), ZOOM_OUT_TOOLTIP.toString(), false);
        grid = initChildCheckBox(GRID_ICON.toString(), GRID_TOOLTIP.toString(), false);
        snap = initChildCheckBox(SNAP_ICON.toString(), SNAP_TOOLTIP.toString(), false);
        viewControls = new ToolBar(zoomIn, zoomOut, grid, snap);
        
        snapshot = initChildButton(SNAPSHOT_ICON.toString(), SNAPSHOT_TOOLTIP.toString(), false);
        code = initChildButton(CODE_ICON.toString(), CODE_TOOLTIP.toString(), false);
        exportControls = new ToolBar(snapshot, code);
        
        spaceBox = new HBox(); spaceBox.getStyleClass().add("spacebox");
        spaceBox2 = new HBox(); spaceBox2.getStyleClass().add("spacebox");
        spaceBox3 = new HBox(); spaceBox3.getStyleClass().add("spacebox");
        
        FlowPane topbox = (FlowPane) appPane.getTop();
        topbox.getChildren().addAll(spaceBox,drawControls,spaceBox2,viewControls,spaceBox3,exportControls);
        
        className = new Label("Class Name:");
        classText = new TextField();
        classBox = new HBox();
        classBox.getChildren().addAll(className, classText);
        
        pkgName = new Label("Package:");
        pkgText = new TextField();
        pkgBox = new HBox();
        pkgBox.getChildren().addAll(pkgName, pkgText);
        
        parentName = new Label("Parent:");
        parentChoice = new ChoiceBox(FXCollections.observableArrayList("None","Custom..."));
        parentChoice.getSelectionModel().selectFirst();
        parentBox = new HBox();
        parentBox.getChildren().addAll(parentName, parentChoice);
        
        interfaceName = new Label("Interface:");
        addInterface = initChildButton(ADD_ICON.toString(), null, false);
        removeInterface = initChildButton(REMOVE_ICON.toString(), null, false);
        interfaceList = new VBox();
        interfaceScroll = new ScrollPane();
        interfaceScroll.setContent(interfaceList);
        interfaceHBox = new HBox();
        interfaceHBox.getChildren().addAll(interfaceName,addInterface,removeInterface);
        interfaceVBox = new VBox();
        interfaceVBox.getChildren().addAll(interfaceHBox,interfaceScroll);
        
        depName = new Label("Dependencies:");
        addDep = initChildButton(ADD_ICON.toString(), null, false);
        removeDep = initChildButton(REMOVE_ICON.toString(), null, false); 
        depList = new VBox();
        depScroll = new ScrollPane();
        depScroll.setContent(depList);
        depHBox = new HBox();
        depHBox.getChildren().addAll(depName,addDep,removeDep);
        depVBox = new VBox();
        depVBox.getChildren().addAll(depHBox,depScroll); 
        
        varName = new Label("Variables:");
        addVar = initChildButton(ADD_ICON.toString(), null, false);
        removeVar = initChildButton(REMOVE_ICON.toString(), null, false);
        varBox = new HBox();
        varBox.getChildren().addAll(varName, addVar, removeVar);
        
        varTableBox = new ScrollPane();
        varTable = new TableView();
        varNameCol = new TableColumn("Name");
        varNameCol.setCellValueFactory(new PropertyValueFactory("name"));
        varTypeCol = new TableColumn("Type");
        varTypeCol.setCellValueFactory(new PropertyValueFactory("type"));
        varStaticCol = new TableColumn("Static");
        varStaticCol.setCellValueFactory(new PropertyValueFactory("isStatic"));
        varAccessCol = new TableColumn("Access");
        varAccessCol.setCellValueFactory(new PropertyValueFactory("visibility"));
        varFinalCol = new TableColumn("Final");
        varFinalCol.setCellValueFactory(new PropertyValueFactory("isFinal"));
        varTable.getColumns().addAll(varNameCol,varTypeCol,varAccessCol,varStaticCol,varFinalCol);
        varTableBox.setContent(varTable);
        varTableBox.setFitToWidth(true);
        
        methodName = new Label("Methods:");
        addMethod = initChildButton(ADD_ICON.toString(), null, false);
        removeMethod = initChildButton(REMOVE_ICON.toString(), null, false);
        methodBox = new HBox();
        methodBox.getChildren().addAll(methodName, addMethod, removeMethod);
        
        methodTableBox = new ScrollPane();
        methodTable = new TableView();
        methodNameCol = new TableColumn("Name");
        methodNameCol.setCellValueFactory(new PropertyValueFactory("name"));
        methodReturnCol = new TableColumn("Return");
        methodReturnCol.setCellValueFactory(new PropertyValueFactory("type"));
        methodStaticCol = new TableColumn("Static");
        methodStaticCol.setCellValueFactory(new PropertyValueFactory("isStatic"));
        methodAccessCol = new TableColumn("Access");
        methodAccessCol.setCellValueFactory(new PropertyValueFactory("visibility"));
        methodAbstractCol = new TableColumn("Abstract");
        methodAbstractCol.setCellValueFactory(new PropertyValueFactory("isAbstract"));
        methodTable.getColumns().addAll(methodNameCol,methodReturnCol,methodAccessCol,methodStaticCol,methodAbstractCol);
        methodTableBox.setContent(methodTable);
        methodTableBox.setFitToWidth(true);
        
        classinfo = new VBox();
        vminfo = new VBox();
        classinfo.getChildren().addAll(classBox,pkgBox,parentBox,interfaceVBox,depVBox);
        vminfo.getChildren().addAll(varBox,varTableBox,methodBox,methodTableBox);
        
        left = new TabPane();
        classTab = new Tab();
        vmTab = new Tab();
        classTab.setContent(classinfo);
        classTab.setClosable(false);
        classTab.setText("Class Information");
        vmTab.setContent(vminfo);
        vmTab.setClosable(false);
        vmTab.setText("Variables and Methods");
        left.getTabs().addAll(classTab,vmTab);
        
        center = new ScrollPane();
        canvas = new Pane();
        
        //FIXME
        canvas.setMinSize(500, 500);
        
        center.setContent(canvas);

        // AND NOW SETUP THE WORKSPACE
	workspace = new BorderPane();
	((BorderPane)workspace).setLeft(left);
	((BorderPane)workspace).setCenter(center);

    }
    
    private void initializeHandlers() {
        
        appController = new AppController(app);
        canvasController = new CanvasController(app);
        elementController = new ElementController(app);
        //TOOLBAR
        select.setOnAction(e -> {
            appController.selectMode();
        });
        remove.setOnAction(e -> {
            canvasController.removeJT();
        });
        resize.setOnAction(e -> {
            JavaType selected = dataManager.getSelected();
            if(selected!=null) {
                selected.resizeModeOn();
            }
        });
        classB.setOnAction(e->{
            appController.addMode();
            addingClass = true;
        });
        interfaceB.setOnAction(e->{
            appController.addMode();
            addingClass = false;
        });
        code.setOnAction(e -> {
            appController.exportCode(gui.getWindow());
        });
        snapshot.setOnAction(e -> {
            appController.exportPhoto(gui.getWindow(), canvas);
        });
        zoomIn.setOnAction(e -> {
            if(zoom < 2.5) {
                zoom += 0.1;
            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Warning!");
                alert.setHeaderText("Max Zoom!");
                alert.setContentText("You cannot zoom in further.");
                alert.showAndWait();
            }
            canvas.setScaleX(zoom);
            canvas.setScaleY(zoom);
        });
        zoomOut.setOnAction(e -> {
            if(zoom > 0) {
                zoom -= 0.1;
            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Warning!");
                alert.setHeaderText("Min Zoom!");
                alert.setContentText("You cannot zoom out further.");
                alert.showAndWait();
            }
            canvas.setScaleX(zoom);
            canvas.setScaleY(zoom);
        });
        snap.setOnAction(e->{
            dataManager.setSnapMode(snap.isSelected());
        });
        grid.setOnAction(e->{
            if(grid.isSelected()) {
                gridlines = new ArrayList();
            } else if(gridlines != null) {
                gridlines = null;
            }
            reloadWorkspace();
        });
        
        //CANVAS
        canvas.setOnMouseClicked(e -> {
            if(dataManager.isInState(AppState.ADD_MODE)){
                double x = e.getX();
                double y = e.getY();
                if(dataManager.getSnapMode()) {
                    x = 25*(Math.round(x/25));
                    y = 25*(Math.round(y/25));                
                }
                if(addingClass) {
                    canvasController.addClass(x,y,null,true);
                } else {
                    canvasController.addInterface(x,y,null,true);
                } 
            }
        });
        
        //CLASS INFO 
        classText.textProperty().addListener((o, oldval, newval) -> {
            elementController.updateName(newval);
        });
        pkgText.textProperty().addListener((o, oldval, newval) -> {
            elementController.updatePkg(newval);
        });
        parentChoice.getSelectionModel().selectedItemProperty().addListener((o, oldval, newval)->{
            if(newval!=null) {
                if(newval.equals("Custom...")) {
                    TextInputDialog customDialog = new TextInputDialog("");
                    customDialog.setTitle("Custom Parent");
                    customDialog.setHeaderText("This dialog can be used to enter a custom parent class.");
                    customDialog.setContentText("Parent name: ");
                    Optional<String> customParent = customDialog.showAndWait();
                    if (customParent.isPresent()){
                        newval = customParent.get();
                    }
                }
                elementController.setParent(newval.toString());
            }
        });
        addInterface.setOnAction(e -> {
            elementController.addInterface();
        });
        removeInterface.setOnAction(e -> {
            elementController.removeInterface();
        });
        addDep.setOnAction(e->{
            elementController.addDep();
        });
        removeDep.setOnAction(e->{
            elementController.removeDep();
        });
        varTable.setRowFactory(c -> {
            TableRow<VariableJT> varRow = new TableRow();
            varRow.setOnMouseClicked(e -> {
                if(e.getClickCount() == 2 && !varRow.isEmpty()) {
                    VariableJT current = varRow.getItem();
                    VariableDialog dialog = new VariableDialog(gui.getWindow(),varTable,dataManager.getSelected(),"Edit",dataManager);
                    dialog.show("Edit Variable", current);
                }
            });
            return varRow;
        });
        addVar.setOnAction(e->{
            elementController.addVariable(varTable,gui.getWindow());
        });
        removeVar.setOnAction(e->{
            elementController.removeVariable(varTable);
        });
        methodTable.setRowFactory(c -> {
            TableRow<MethodJT> methodRow = new TableRow();
            methodRow.setOnMouseClicked(e -> {
                if(e.getClickCount() == 2 && !methodRow.isEmpty()) {
                    MethodJT current = methodRow.getItem();
                    MethodDialog dialog = new MethodDialog(gui.getWindow(),varTable,dataManager.getSelected(),"Edit",dataManager);
                    dialog.show("Edit Method", current);
                }
            });
            return methodRow;
        });
        addMethod.setOnAction(e->{
            elementController.addMethod(methodTable,gui.getWindow());
        });
        removeMethod.setOnAction(e->{
            elementController.removeMethod(methodTable);
        });
        
        
    }
    
    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    //TODO
    public void initStyle() {
        /*
        left.getStyleClass().add(CLASS_LEFTBOX);
        pkgName.getStyleClass().addAll(CLASS_LABEL,CLASS_H1);
        className.getStyleClass().addAll(CLASS_LABEL,CLASS_H1);
        parentName.getStyleClass().addAll(CLASS_LABEL,CLASS_H1);
        varName.getStyleClass().addAll(CLASS_LABEL,CLASS_H1);
        methodName.getStyleClass().addAll(CLASS_LABEL,CLASS_H1);
        addVar.getStyleClass().add(CLASS_FILE_BUTTON);
        removeVar.getStyleClass().add(CLASS_FILE_BUTTON);
        addMethod.getStyleClass().add(CLASS_FILE_BUTTON);
        removeMethod.getStyleClass().add(CLASS_FILE_BUTTON);
        classText.getStyleClass().add(CLASS_H2);
        pkgText.getStyleClass().add(CLASS_H2);
        parentChoice.getStyleClass().add(CLASS_H2);
        classBox.setStyle("-fx-spacing: 50;");
        pkgBox.setStyle("-fx-spacing: 86;");
        parentBox.setStyle("-fx-spacing: 108;");
        for(Node n : left.getChildren()) {
            n.getStyleClass().add(CLASS_EDITBOX);
        }
        
        canvas.getStyleClass().add(CLASS_CANVAS);
        center.getStyleClass().add(CLASS_CANVAS);
        //Toolbar styling
        drawControls.getStyleClass().add(CLASS_TOOLBAR);
        drawControls.setOnMouseEntered(e -> {
               drawControls.getStyleClass().add(CLASS_TOOLBAR_HOVER);
        });
        drawControls.setOnMouseExited(e -> {
            drawControls.getStyleClass().remove(CLASS_TOOLBAR_HOVER);
        });
        viewControls.getStyleClass().add(CLASS_TOOLBAR);
        viewControls.setOnMouseEntered(e -> {
               viewControls.getStyleClass().add(CLASS_TOOLBAR_HOVER);
        });
        viewControls.setOnMouseExited(e -> {
            viewControls.getStyleClass().remove(CLASS_TOOLBAR_HOVER);
        });
        exportControls.getStyleClass().add(CLASS_TOOLBAR);
        exportControls.setOnMouseEntered(e -> {
               exportControls.getStyleClass().add(CLASS_TOOLBAR_HOVER);
        });
        exportControls.setOnMouseExited(e -> {
            exportControls.getStyleClass().remove(CLASS_TOOLBAR_HOVER);
        });
        
        
        //Button styling
        for(int i = 0; i < drawControls.getItems().size(); i++) {
            Button b = (Button)drawControls.getItems().get(i);
            b.getStyleClass().add(CLASS_FILE_BUTTON);
            b.setOnMouseEntered(e -> {
                b.getStyleClass().add(CLASS_FILE_BUTTON_HOVER);
            });
            b.setOnMouseExited(e -> {
                b.getStyleClass().remove(CLASS_FILE_BUTTON_HOVER);
            });
        }
        for(int i = 0; i < viewControls.getItems().size(); i++) {
            Node b = viewControls.getItems().get(i);
            b.getStyleClass().add(CLASS_FILE_BUTTON);
            b.setOnMouseEntered(e -> {
                b.getStyleClass().add(CLASS_FILE_BUTTON_HOVER);
            });
            b.setOnMouseExited(e -> {
                b.getStyleClass().remove(CLASS_FILE_BUTTON_HOVER);
            });
        }
        for(int i = 0; i < exportControls.getItems().size(); i++) {
            Button b = (Button)exportControls.getItems().get(i);
            b.getStyleClass().add(CLASS_FILE_BUTTON);
            b.setOnMouseEntered(e -> {
                b.getStyleClass().add(CLASS_FILE_BUTTON_HOVER);
            });
            b.setOnMouseExited(e -> {
                b.getStyleClass().remove(CLASS_FILE_BUTTON_HOVER);
            });
        }
        */
    }
    
    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {
        JavaType selected = dataManager.getSelected();
        //UPDATE CANVAS ELEMENTS
        canvas.getChildren().clear();       
        if(gridlines != null) {
            int rows = (int)canvas.getHeight()/25;
            int cols = (int)canvas.getWidth()/25;
            int i; double j;
            for(j = canvas.getLayoutX(); j <= canvas.getWidth(); j += 25) {
                Line line = new Line(j,canvas.getLayoutX(),j,canvas.getLayoutX()+canvas.getHeight());
                line.setStroke(Color.web("#BFBFBF"));
                line.setStrokeWidth(1);
                gridlines.add(line);
            }
            for(j = canvas.getLayoutY(); j <= canvas.getHeight(); j += 25) {
                Line line = new Line(canvas.getLayoutY(),j,canvas.getLayoutY()+canvas.getWidth(),j);
                line.setStroke(Color.web("#BFBFBF"));
                line.setStrokeWidth(1);
                gridlines.add(line);
            }
            for(i = 0; i < gridlines.size(); i++) {
                canvas.getChildren().add(gridlines.get(i));
            }
        } 
        canvas.getChildren().addAll(dataManager.getGUIElements());
        for(int i = 0; i < dataManager.getElements().size(); i++) {
            dataManager.getElements().get(i).selectEffectOff();
            dataManager.getElements().get(i).resizeModeOff();
        }
        dataManager.renderLines();
        if(selected!=null) {
            selected.selectEffectOn();
        }
        //PARENT CHOICE BOX UPDATE
        ArrayList<String> choiceData = new ArrayList();
        choiceData.add(0, "None");
        choiceData.addAll(dataManager.getParentNames());
        choiceData.add("Custom...");
        parentChoice.setItems(FXCollections.observableArrayList(choiceData));
        if(selected != null) {
            String parent = selected.getParent();
            if(!parent.equals("")) {
                parentChoice.getSelectionModel().select(parent);
            } else {
                parentChoice.getSelectionModel().selectFirst();
            }
        } else {
            parentChoice.getSelectionModel().selectFirst();
        }
        //INTERFACE LIST UPDATE
        interfaceList.getChildren().clear();
        if(selected != null) {
            for(String s : selected.getInterfaceNames()) {
                Label interfaceName = new Label(s);
                interfaceList.getChildren().add(interfaceName);
            }
        }
        //DEPENDENCY UPDATE
        depList.getChildren().clear();
        if(selected != null) {
            for(String s : selected.getDependencies()) {
                Label depName = new Label(s);
                depList.getChildren().add(depName);
            }
        }
        //TABLE UPDATE
        if(selected != null) {
            varTable.setItems(selected.getVarsData());
            methodTable.setItems(selected.getMethodsData());
        }
        //UPDATE CONTROLS
        if(dataManager.isInState(AppState.ADD_MODE)) {
            classTab.setDisable(true);
            vmTab.setDisable(true);
        } else if (dataManager.isInState(AppState.SELECT_MODE)) {
            classTab.setDisable(false);
            vmTab.setDisable(false);
            if(selected != null) {
                classText.setText(selected.getName());
                pkgText.setText(selected.getPkg());
            } else {
                classText.setText("");
                pkgText.setText("");
            }
            
        } else {
            classTab.setDisable(true);
            vmTab.setDisable(true);
        }
    }
    
    //Enable/disable buttons here
    public void updateButtonStat() {
        if(dataManager.isInState(AppState.ADD_MODE)) {
            classB.getStyleClass().add(CLASS_FILE_BUTTON_SELECT);
            select.getStyleClass().remove(CLASS_FILE_BUTTON_SELECT);
            canvas.setCursor(Cursor.CROSSHAIR);
        } else if (dataManager.isInState(AppState.SELECT_MODE)) {
            select.getStyleClass().add(CLASS_FILE_BUTTON_SELECT);
            classB.getStyleClass().remove(CLASS_FILE_BUTTON_SELECT);
            canvas.setCursor(Cursor.HAND);
        } else {
            canvas.setCursor(Cursor.DEFAULT);
        }
    }
    
    public Button initChildButton(String icon, String tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	// LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(icon);
        Image buttonImage = new Image(imagePath);
	// NOW MAKE THE BUTTON
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        if(tooltip!=null) {
          Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip));
          button.setTooltip(buttonTooltip);  
        }
	// AND RETURN THE COMPLETED BUTTON
        return button;
    }
    
    public CheckBox initChildCheckBox(String icon, String tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	// LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(icon);
        Image buttonImage = new Image(imagePath);
	// NOW MAKE THE BUTTON
        CheckBox checkbox = new CheckBox();
        checkbox.setDisable(disabled);
        checkbox.setGraphic(new ImageView(buttonImage));
        Tooltip checkBoxTooltip = new Tooltip(props.getProperty(tooltip));
        checkbox.setTooltip(checkBoxTooltip);
	// AND RETURN THE COMPLETED BUTTON
        return checkbox;
    }

    public Pane getCanvas() {
        return canvas;
    }
    
    public VBox getInterfaceList() {
        return interfaceList;
    }
    
}

