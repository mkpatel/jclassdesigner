package jcd;

/**
 * These are properties that are to be loaded from workspace_properties.xml. They
 * will provide custom labels and other UI details for this application and
 * it's custom workspace. Note that in this application we're using two different
 * properties XML files. simple_app_properties.xml is for settings known to the
 * Simple App Framework and so helps to set it up. These properties would be for
 * anything relevant to this custom application. The reason for loading this stuff
 * from an XML file like this is to make these settings independent of the code
 * and therefore easily interchangeable, like if we wished to change the language
 * the application ran in.
 * 
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public enum PropertyType {
    SELECT_ICON,        SELECT_TOOLTIP,
    RESIZE_ICON,        RESIZE_TOOLTIP,
    CLASS_ICON,         CLASS_TOOLTIP,
    INTERFACE_ICON,     INTERFACE_TOOLTIP,
    DELETE_ICON,        DELETE_TOOLTIP,
    UNDO_ICON,          UNDO_TOOLTIP,
    REDO_ICON,          REDO_TOOLTIP,
    ZOOM_IN_ICON,       ZOOM_IN_TOOLTIP,
    ZOOM_OUT_ICON,      ZOOM_OUT_TOOLTIP,
    SNAPSHOT_ICON,      SNAPSHOT_TOOLTIP,
    CODE_ICON,          CODE_TOOLTIP,
    SNAP_ICON,          SNAP_TOOLTIP,
    GRID_ICON,          GRID_TOOLTIP,
    ADD_ICON,           ADD_TOOLTIP,
    REMOVE_ICON,        REMOVE_TOOLTIP,

    REMOVE_ELEMENT_MESSAGE,
    ATTRIBUTE_UPDATE_ERROR_MESSAGE,
    ATTRIBUTE_UPDATE_ERROR_TITLE,
    UPDATE_ERROR_MESSAGE,
    UPDATE_ERROR_TITLE 
}